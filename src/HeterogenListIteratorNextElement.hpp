#ifndef SDP_CONTAINER_SRC_HETEROGEN_LIST_ITERATOR_NEXT_ELEMENT_HPP
#define SDP_CONTAINER_SRC_HETEROGEN_LIST_ITERATOR_NEXT_ELEMENT_HPP

#include "HeterogenList.hpp"
#include "Iterator.hpp"
#include "ListIterator.hpp"
#include "containers/Container.hpp"
#include "containers/ContainerIterator.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

template <typename T>
class HeterogenList;

template <typename T>
class HeterogenListIteratorNextElement
    : public Iterator<T, HeterogenListIteratorNextElement<T>> {
 private:
  friend class HeterogenList<T>;
  ListIterator<Container<T>*> container;
  ContainerIterator<T> currentElement;

 public:
  HeterogenListIteratorNextElement(ListIterator<Container<T>*> container,
                                   ContainerIterator<T> currentElement)
      : container(container), currentElement(currentElement) {}

  virtual T& Get() const { return currentElement; }
  virtual HeterogenListIteratorNextElement<T> Next() const {
    if (currentElement.HasNext()) {
      return HeterogenListIteratorNextElement<T>(container,
                                                 currentElement.Next());
    }

    ListIterator<Container<T>*> n = container.Next();
    if (container.HasNext()) {
      return HeterogenListIteratorNextElement<T>(n, n.Get()->Begin());
    }

    return HeterogenListIteratorNextElement<T>(n, currentElement.Next());
  }
  virtual HeterogenListIteratorNextElement<T> Prev() const {
    if (currentElement.HasPrev()) {
      return HeterogenListIteratorNextElement<T>(container,
                                                 currentElement.Prev());
    }
    ListIterator<Container<T>*> n = container.Prev();
    return HeterogenListIteratorNextElement<T>(n, n.Get()->Last());
  }
  virtual bool Valid() const { return currentElement.Valid(); }
  virtual bool operator==(const HeterogenListIteratorNextElement<T>& it) const {
    return container == it.container && currentElement == it.currentElement;
  }
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_HETEROGEN_LIST_ITERATOR_NEXT_ELEMENT_HPP