#ifndef SDP_CONTAINER_SRC_CONTAINERS_QUEUE_HPP
#define SDP_CONTAINER_SRC_CONTAINERS_QUEUE_HPP

#include "./Container.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

template <typename T>
class Queue : public Container<T> {
 public:
  Queue() : Container<T>('Q') {}
  virtual T Remove() { return this->list.Remove(0); }
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_CONTAINERS_QUEUE_HPP