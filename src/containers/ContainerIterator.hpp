#ifndef SDP_CONTAINER_SRC_CONTAINERS_CONTAINER_ITERATOR_HPP
#define SDP_CONTAINER_SRC_CONTAINERS_CONTAINER_ITERATOR_HPP

#include "../Iterator.hpp"
#include "../ListIterator.hpp"
#include "../Node.hpp"
#include "Container.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

template <typename T>
class Container;

/**
 * Iterator for containers
 */
template <typename T>
class ContainerIterator : public Iterator<T, ContainerIterator<T>> {
 private:
  friend class Container<T>;
  ListIterator<T> it;

 public:
  ContainerIterator(ListIterator<T> it) : it(it) {}

  virtual T& Get() const { return (T&)it; }
  virtual ContainerIterator<T> Next() const {
    return ContainerIterator(it.Next());
  }
  virtual ContainerIterator<T> Prev() const {
    return ContainerIterator(it.Prev());
  }
  virtual bool Valid() const { return it.Valid(); }
  virtual bool operator==(const ContainerIterator<T>& iter) const {
    return it.operator==(iter.it);
  }
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_CONTAINERS_CONTAINER_ITERATOR_HPP