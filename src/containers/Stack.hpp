#ifndef SDP_CONTAINER_SRC_CONTAINERS_STACK_HPP
#define SDP_CONTAINER_SRC_CONTAINERS_STACK_HPP

#include "./Container.hpp"

using eu::petartoshev::fmi::sdp::containers::Container;

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

template <typename T>
class Stack : public Container<T> {
 public:
  Stack() : Container<T>('S') {}
  virtual bool Add(T& element) { return this->list.Add(element, 0); }
  virtual void Print(std::ostream& out) const {
    out << this->type << ' ';
    this->list.PrintReverse(out);
    out << std::endl;
  }
  virtual T Remove() { return this->list.Remove(0); }
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_CONTAINERS_STACK_HPP