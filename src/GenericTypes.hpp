#ifndef SDP_CONTAINER_SRC_GENERIC_TYPES_HPP
#define SDP_CONTAINER_SRC_GENERIC_TYPES_HPP

/**
 * Generic types for whole project
 */

#define uint unsigned int
#define ushort unsigned short
#define unum ushort
#define CONDITION_TYPED(T) std::function<bool(const T&)>
#define CONDITION CONDITION_TYPED(T)

#endif  // SDP_CONTAINER_SRC_GENERIC_TYPES_HPP