#ifndef SDP_CONTAINER_SRC_HETEROGEN_LIST_HPP
#define SDP_CONTAINER_SRC_HETEROGEN_LIST_HPP

#include <fstream>
#include <functional>
#include <stdexcept>

#include "GenericTypes.hpp"
#include "HeterogenListIteratorNextElement.hpp"
#include "HeterogenListIteratorSequence.hpp"
#include "LinkedList.hpp"
#include "ListIterator.hpp"
#include "Node.hpp"
#include "containers/Container.hpp"
#include "containers/DoubleLinkedList.hpp"
#include "containers/Queue.hpp"
#include "containers/Stack.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

template <typename T>
class HeterogenList {
 public:
  /// import data from file to current list
  bool Read(const char* filename);
  /// Load balanced add
  bool Add(T& element) {
    Container<T>* min = this->list.Begin();
    for (auto it = this->list.Begin(); it != this->list.End(); it++)
      if (it->GetSize() < min->GetSize()) min = it;
    return min->Add(element);
  }
  bool AddToContainer(const unum containerIndex, int& element) {
    CheckIndex(containerIndex);
    return this->list.Get(containerIndex)->Add(element);
  }
  bool Contains(const T& el) {
    bool found = false;
    ForEach([&](Container<T>* elem) { found = found || elem->Contains(el); });
    return found;
  }
  void Filter(CONDITION predicate) {
    ForEach([&predicate](Container<T>* elem) { elem->Filter(predicate); });
  }
  HeterogenListIteratorNextElement<T> Find(CONDITION predicate) const {
    auto it = BeginNextElement();
    while (it.Valid() && !predicate(it)) it++;
    return it;
  }
  HeterogenListIteratorNextElement<T> Find(const T& elem) const {
    return Find([&elem](const T& val) { return elem == val; });
  }
  void Print(std::ostream& out) const {
    ForEach([&out](Container<T>* elem) { elem->Print(out); });
  }
  Container<T>* RemoveFromContainer(const unum containerIndex) {
    CheckIndex(containerIndex);
    return this->list.Get(containerIndex)->Remove();
  }
  bool Save(const char* filename) {
    std::ofstream out(filename);
    if (!out.is_open()) {
      std::cerr << "Cannot open file " << filename;
      return false;
    }
    this->Print(out);
    out.close();
    return true;
  }
  void SortContainers() {
    ForEach([](Container<T>* elem) { elem->Sort(); });
  }

  // Container iterators
  ListIterator<Container<T>*> BeginContainers() const {
    return this->list.Begin();
  }
  ListIterator<Container<T>*> EndContainers() const { return this->list.End(); }
  ListIterator<Container<T>*> LastContainers() const {
    return this->list.Last();
  }

  // Next element iterators
  HeterogenListIteratorNextElement<T> BeginNextElement() const {
    return HeterogenListIteratorNextElement<T>(
        this->list.Begin(), (this->list.Begin().Get())->Begin());
  }
  HeterogenListIteratorNextElement<T> EndNextElement() const {
    return HeterogenListIteratorNextElement<T>(this->list.End(),
                                               this->list.Begin().Get()->End());
  }
  HeterogenListIteratorNextElement<T> LastNextElement() const {
    return HeterogenListIteratorNextElement<T>(
        this->list.Last(), (this->list.Last().Get())->Last());
  }

  // Sequence iterators
  // HeterogenListIteratorSequence<T> BeginSequence() const {
  //   SortContainers();
  //   ListIterator<Container<T>*> minCtrIter = BeginContainers();
  //   for (auto ctrIter = BeginContainers(); ctrIter != EndContainers();
  //        ++ctrIter) {
  //     if (((Container<T>*)minCtrIter)->Begin().Get() >
  //         ((Container<T>*)ctrIter)->Begin().Get()) {
  //       minCtrIter = ctrIter;
  //     }
  //   }
  //   return HeterogenListIteratorSequence<T>(
  //       BeginContainers(), minCtrIter, minCtrIter,
  //       ((Container<T>*)minCtrIter)->Begin(),
  //       ((Container<T>*)minCtrIter)->Begin().Get());
  // }

  // HeterogenListIteratorSequence<T> EndSequence() const {
  //   return HeterogenListIteratorSequence<T>(
  //       BeginContainers(), BeginContainers().Prev(),
  //       BeginContainers().Prev(),
  //       ((Container<T>*)BeginContainers())->End(),
  //       ((Container<T>*)BeginContainers())->Begin().Get());
  // }

  // TODO: Petar Toshev 18.11.2019 Create templated function for deleting
  // Container and remove Template from Heterogen List

 private:
  void CheckIndex(const unum& containerIndex) {
    if (this->list.GetSize() <= containerIndex || containerIndex < 0)
      throw std::invalid_argument("Wrong container index: " + containerIndex);
  }
  Container<T>* CreateNewContainer(const char& containerType) {
    switch (containerType) {
      case 'D':
        return new DoubleLinkedList<T>;
      case 'S':
        return new Stack<T>;
      case 'Q':
        return new Queue<T>;

      default:
        throw std::invalid_argument("Wrong container type: " + containerType);
    }
  }
  void ForEach(std::function<void(Container<T>*)> manipulate) const {
    for (auto it = this->list.Begin(); it != this->list.End(); it++) {
      manipulate(it);
    }
  }
  void ForEach(std::function<void(Container<T>*)> manipulate) {
    for (auto it = this->list.Begin(); it != this->list.End(); it++) {
      manipulate(it);
    }
  }

  LinkedList<Container<T>*> list;
  // eu::petartoshev::fmi::sdp::containers::LinkedList<void*> listyyyyy(false);
};  // namespace containers

template <typename T>
bool HeterogenList<T>::Read(const char* filename) {
  std::ifstream dataStream(filename);
  // Open the File
  if (dataStream.is_open()) {
    char peek;
    char containerType;
    T i;
    while (dataStream.peek() != EOF) {
      // read container type
      dataStream >> containerType;
      Container<T>* container = CreateNewContainer(containerType);
      this->list.Add(container);

      // read container data
      while ((peek = dataStream.peek()) != '\n' && peek != EOF) {
        dataStream >> i;
        container->Add(i);
      }
      dataStream.ignore();
    }
    dataStream.close();
    return true;
  }
  return false;
}

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_HETEROGEN_LIST_HPP