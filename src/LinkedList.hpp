#ifndef SDP_CONTAINER_SRC_LINKED_LIST_HPP
#define SDP_CONTAINER_SRC_LINKED_LIST_HPP

#include <stdexcept>
#include <type_traits>

#include "GenericTypes.hpp"
#include "List.hpp"
#include "ListIterator.hpp"
#include "Node.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

template <typename T>
class ListIterator;

/**
 * Template linked list
 */
template <typename T>
class LinkedList : public List<T> {
 public:
  LinkedList(const bool deleteElementsDataOnDestruction = false)
      : deleteElementsDataOnDestruction(deleteElementsDataOnDestruction) {}

  virtual bool Add(T&, const unum);
  virtual bool Add(T& element) { return this->Add(element, this->size); }
  virtual bool Contains(const T& el) const {
    return Contains([&](const T& elem) { return el == elem; });
  }
  virtual bool Contains(CONDITION predicate) const {
    Node<T>* curr = first;
    while (curr != nullptr && !predicate(curr->Get())) curr = curr->GetNext();
    return curr != nullptr;
  }
  virtual void Clear() {
    Node<T>* curr = first;
    while (curr != nullptr) {
      Node<T>* next = curr->GetNext();
      // TODO: Petar Toshev 26.11.2019 Enable deleting on list which are from
      // poniters if (this->deleteElementsDataOnDestruction &&
      // std::is_pointer<T>::value)
      //   delete curr->Get();
      delete curr;
      curr = next;
    }
    first = nullptr;
    size = 0;
  }
  virtual void Filter(CONDITION predicate) {
    Node<T>* curr = first;
    while (curr != nullptr) {
      if (predicate(curr->Get())) RemoveNode(curr);
      curr = curr->GetNext();
    }
  }
  virtual const T& Get(unum index) const {
    CheckSize(index);
    return GetNode(index)->Get();
  }
  virtual unum GetSize() const { return this->size; }
  virtual bool IsEmpty() const { return this->size == 0; }
  virtual void Print(std::ostream& out) const {
    Node<T>* curr = first;
    if (curr != nullptr) out << (curr = curr->GetNext())->GetPrev()->Get();
    while (curr != nullptr) {
      out << ' ' << curr->Get();
      curr = curr->GetNext();
    }
  }
  virtual void PrintReverse(std::ostream& out) const {
    Node<T>* curr = last;
    if (curr != nullptr) out << (curr = curr->GetPrev())->GetNext()->Get();
    while (curr != nullptr) {
      out << ' ' << curr->Get();
      curr = curr->GetPrev();
    }
  }
  virtual void Sort() {
    if (this->size < 2) return;
    const unum halfSize = this->size / 2;
    if (this->first->Get() > this->last->Get()) this->last = this->first;
    this->first = MergeSort(this->first, this->size);
    while (this->last->GetNext() != nullptr) this->last = this->last->GetNext();
  }
  virtual T Remove(const unum index) {
    CheckSize(index);
    Node<T>* to_delete = RemoveNode(GetNode(index));
    T res = to_delete->Get();
    delete to_delete;
    return res;
  }
  void Reverse();
  ListIterator<T> Begin() const { return ListIterator<T>(this->first); }
  ListIterator<T> End() const { return ListIterator<T>(); }
  ListIterator<T> Last() const { return ListIterator<T>(this->last); }
  // friend std::ostream& operator<<(std::ostream& os,
  //                                 const LinkedList<T>& list) {
  //   os << "LIST: ";
  //   Node<T>* ptr = list.first;
  //   while (ptr != nullptr) {
  //     os << " [" << ptr->Get() << "] ->";
  //     ptr = ptr->GetNext();
  //   }
  //   return os;
  // }

 private:
  void CheckSize(const unum index, const bool enable_equal = true) const {
    if (index > this->size || (enable_equal && index == this->size)) {
      throw std::out_of_range("too big index");
    }
  }
  void Copy(const LinkedList<T>& l);
  Node<T>* GetNode(const unum index) { return GetNodeHelper(index); }
  Node<T>* GetNode(const unum index) const { return GetNodeHelper(index); }
  Node<T>* GetNodeHelper(const unum index) const {
    if (index > this->size / 2) {
      return GetNodeHelper(this->size - index - 1, this->last,
                           Node<T>::GetPrevOnNode);
    }
    return GetNodeHelper(index, this->first, Node<T>::GetNextOnNode);
  }
  Node<T>* GetNodeHelper(unum remaining, Node<T>* curr,
                         decltype(Node<T>::GetNextOnNode) next) const {
    while (remaining-- >= 1) curr = next(curr);
    return curr;
  }
  void InsertAfter(Node<T>* element, Node<T>* location) {
    Node<T>* next = location->GetNext();
    location->SetNext(element);
    element->SetPrev(location);
    element->SetNext(next);
    SetPrevIfNotNull(next, element);
    this->size++;
  }
  void InsertBefore(Node<T>* element, Node<T>* location) {
    Node<T>* prev = location->GetPrev();
    location->SetPrev(element);
    element->SetNext(location);
    element->SetPrev(prev);
    if (prev != nullptr) prev->SetNext(element);
    this->size++;
  }
  Node<T>* MergeSort(Node<T>* head, const unum len) {
    if (len <= 1) return head;
    const unum halfLen = len / 2;
    Node<T>* b = head;
    for (unum i = 0; i < halfLen; i++) b = b->GetNext();
    return SortedMerge(MergeSort(head, halfLen), halfLen,
                       MergeSort(b, len - halfLen), len - halfLen);
  }
  void SetPrevIfNotNull(Node<T>* element, Node<T>* value) {
    if (element != nullptr) element->SetPrev(value);
  }
  Node<T>* SortedMerge(Node<T>* al, unum alen, Node<T>* b, unum blen);
  Node<T>* RemoveNode(Node<T>* node);

  unum size = 0;
  Node<T>* first = nullptr;
  Node<T>* last = nullptr;
  const bool deleteElementsDataOnDestruction = false;
};

template <typename T>
bool LinkedList<T>::Add(T& element, const unum index) {
  CheckSize(index, false);  // false to Remove equality from equation
  if (index == 0) {
    Node<T>* n = new Node<T>(nullptr, element, first);
    SetPrevIfNotNull(first, n);
    first = n;
    if (this->size == 0) {
      last = first;
    }
  } else if (index == this->size) {
    Node<T>* n = new Node<T>(last, element);
    last->SetNext(n);
    last = n;
  } else {
    Node<T>* prev = GetNode(index - 1);
    Node<T>* next = prev->GetNext();
    Node<T>* n = new Node<T>(prev, element, next);
    prev->SetNext(n);
    SetPrevIfNotNull(next, n);
  }
  this->size++;
  return true;
}

template <typename T>
void LinkedList<T>::Copy(const LinkedList<T>& l) {
  Node<T>* currL = l.first;
  if (currL != nullptr) {
    first = new Node<T>(nullptr, currL->Get());
    currL = currL->GetNext();
    Node<T>* currC = first;
    while (currL != nullptr) {
      Node<T>* newNode = new Node<T>(currC, currL->Get());
      currC->SetNext(newNode);
      currC = newNode;
      currL = currL->GetNext();
    }
  }
}

template <typename T>
Node<T>* LinkedList<T>::SortedMerge(Node<T>* a, unum alen, Node<T>* b,
                                    unum blen) {
  Node<T>* head = a->Get() <= b->Get() ? a : b;

  // we don't need to move elements after one of list (a or b) is empty,
  // because we move whole nodes and use the same list for sorting
  Node<T>* next;
  while (alen > 0 && blen > 0 && a != nullptr && b != nullptr) {
    if (a->Get() <= b->Get()) {
      a = a->GetNext();
      alen--;
    } else {
      next = b->GetNext();
      this->InsertBefore(this->RemoveNode(b), a);
      blen--;
      b = next;
    }
  }
  return head;
}

template <typename T>
Node<T>* LinkedList<T>::RemoveNode(Node<T>* node) {
  Node<T>* prev = node->GetPrev();
  Node<T>* next = node->GetNext();
  if (prev == nullptr) {  // node is first
    first = next;
  } else {
    prev->SetNext(next);
  }

  if (next == nullptr) {  // node is last
    last = prev;
  } else {
    next->SetPrev(prev);
  }

  this->size--;
  return node;
}

template <typename T>
void LinkedList<T>::Reverse() {
  if (this->size == 0) return;
  Node<T>* curr = this->first;
  Node<T>* tmp;
  while (curr != nullptr) {
    tmp = curr->GetNext();
    curr->SetNext(curr->GetPrev());
    curr->SetPrev(tmp);
    curr = tmp;
  }
  tmp = this->first;
  this->first = this->last;
  this->last = tmp;
}

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_LINKED_LIST_HPP