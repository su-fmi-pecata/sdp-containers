#ifndef SDP_CONTAINER_TEST_CONATINERS_CONTAINER_TEST_HPP
#define SDP_CONTAINER_TEST_CONATINERS_CONTAINER_TEST_HPP

#include <gtest/gtest.h>

#include <algorithm>
#include <exception>

#include "../../src/containers/Container.hpp"
#include "../TypedTestsHelper.hpp"

using eu::petartoshev::fmi::sdp::containers::Container;

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {
namespace test {

template <typename T>
class ContainerTests : public TypedTestHelper<T> {
 public:
  ContainerTests(Container<T>& container) : container(container) {}
  ~ContainerTests() { delete &(this->container); }
  Container<T>& container;
};

}  // namespace test
}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_TEST_CONATINERS_CONTAINER_TEST_HPP