cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(SDP_Heterogen_List_Containers VERSION 0.1)


# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)
 
## add all cpp as executable
file(
    GLOB_RECURSE
    MY_SOURCE_FILES
    src/*
)
add_executable(main ${MY_SOURCE_FILES})

# set magic variable :D
if(CMAKE_BUILD_TYPE)
else()
    set(CMAKE_BUILD_TYPE "PROJECT_DEFAULT_BUILD_VALUE")
endif()

# Add Tests only when in debug mode
if("${CMAKE_BUILD_TYPE} " STREQUAL "Debug ")
	if(UPDATE_GOOGLE_TESTS) # download google test if needed
		# Download and unpack googletest at configure time
		configure_file(CMakeLists.txt.in googletest-download/CMakeLists.txt)
		execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
			RESULT_VARIABLE result
			WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )

		if(result)
			message(FATAL_ERROR "CMake step for googletest failed: ${result}")
		endif()

		execute_process(COMMAND ${CMAKE_COMMAND} --build .
			RESULT_VARIABLE result
			WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )

		if(result)
			message(FATAL_ERROR "Build step for googletest failed: ${result}")
		endif()
	endif()
	# Prevent overriding the parent project's compiler/linker
	# settings on Windows
	set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

	# Add googletest directly to our build. This defines
	# the gtest and gtest_main targets.
	add_subdirectory(${CMAKE_CURRENT_BINARY_DIR}/googletest-src
	                 ${CMAKE_CURRENT_BINARY_DIR}/googletest-build
	                 EXCLUDE_FROM_ALL)

	# The gtest/gtest_main targets carry header search path
	# dependencies automatically when using CMake 2.8.11 or
	# later. Otherwise we have to add them here ourselves.
	if (CMAKE_VERSION VERSION_LESS 2.8.11)
	  include_directories("${gtest_SOURCE_DIR}/include")
	endif()
	
	# tell CMake to use CTest extension
	enable_testing()

	# Add all files from test dir
	file(
    	    GLOB_RECURSE
    	    PROJECT_TEST_FILES
    	    test/*
	)
 
	set(TEST_EXECUTABLE test_project)
	add_executable(${TEST_EXECUTABLE} ${PROJECT_TEST_FILES})
	target_link_libraries(${TEST_EXECUTABLE} gtest_main)
	target_link_libraries(${TEST_EXECUTABLE} gtest)
	
	include(GoogleTest)
	gtest_discover_tests(${TEST_EXECUTABLE})
	add_custom_target(tester COMMAND ${CMAKE_CTEST_COMMAND})
endif()
