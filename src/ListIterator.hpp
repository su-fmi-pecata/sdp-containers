#ifndef SDP_CONTAINER_SRC_LIST_ITERATOR_HPP
#define SDP_CONTAINER_SRC_LIST_ITERATOR_HPP

#include "Iterator.hpp"
#include "LinkedList.hpp"
#include "Node.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

template <typename T>
class LinkedList;

/**
 * Iterator for templated linked list
 */
template <typename T>
class ListIterator : public Iterator<T, ListIterator<T>> {
 private:
  friend class LinkedList<T>;
  Node<T>* node;

 public:
  ListIterator(Node<T>* ptr = nullptr) : node(ptr) {}

  virtual T& Get() const { return node->Get(); }
  virtual ListIterator<T> Next() const {
    return ListIterator<T>(node->GetNext());
  }
  virtual ListIterator<T> Prev() const {
    return ListIterator<T>(node->GetPrev());
  }
  virtual bool Valid() const { return node != nullptr; }
  virtual bool operator==(const ListIterator<T>& it) const {
    return node == it.node;
  }
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_LIST_ITERATOR_HPP