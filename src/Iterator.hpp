#ifndef SDP_CONTAINER_SRC_ITERATOR_HPP
#define SDP_CONTAINER_SRC_ITERATOR_HPP

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

/**
 * Iterator base
 */
template <typename T, typename Iter>
class Iterator {
 public:
  T const& GetConst() const { return Get(); }
  bool HasNext() const { return Valid() && Next().Valid(); }
  bool HasPrev() const { return Valid() && Prev().Valid(); }

  virtual T& Get() const = 0;
  virtual Iter Next() const = 0;
  virtual Iter Prev() const = 0;
  virtual bool Valid() const = 0;

  operator bool() const { return Valid(); }
  operator T&() const { return Get(); }

  Iter& operator++() { return (*((Iter*)this) = Next()); }
  Iter operator++(int) {
    Iter save = *((Iter*)this);
    ++(*this);
    return save;
  }
  Iter operator+(const uint n) {
    Iter iter = *((Iter*)this);
    for (uint i = 0; i < n; i++) ++iter;
    return iter;
  }
  Iter& operator+=(const uint n) { (*((Iter*)this) = operator+(n)); }
  Iter& operator--() { return (*((Iter*)this) = Prev()); }
  Iter operator--(int) {
    Iter save = *((Iter*)this);
    --(*this);
    return save;
  }
  Iter operator-(const uint n) {
    Iter iter = *((Iter*)this);
    for (uint i = 0; i < n; i++) --iter;
    return iter;
  }
  Iter& operator-=(const uint n) { (*((Iter*)this) = operator-(n)); }
  virtual bool operator==(const Iter& it) const = 0;
  bool operator!=(const Iter& it) const { return !operator==(it); }
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_ITERATOR_HPP