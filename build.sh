#!/bin/bash

current_dir="$(pwd)"
script_dir="$(pwd)/`dirname "$0"`"

# read the options
# set all options to be optional and check them after extraction
eval set -- "$(/usr/bin/getopt -o 'cdg' --long 'clear,debug,update-gtest' -- $@)"

CMAKE_ARGS=''

# extract options and their arguments into variables.
while true ; do
    case "$1" in
        -c|--clear)
            clear="TRUE"; shift 1;;
        -d|--debug)
            CMAKE_ARGS+='-DCMAKE_BUILD_TYPE=Debug '; shift 1;;
        -g|--update-gtest)
            CMAKE_ARGS+='-DUPDATE_GOOGLE_TESTS=TRUE '; shift 1;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

FOLDER_NAME='build'

cd "$script_dir"
if [ "${clear}" == "TRUE" ]; then rm -rf "${FOLDER_NAME}"; fi
if [ ! -d ${FOLDER_NAME}  ]; then mkdir  "${FOLDER_NAME}"; fi
if [ ! -d ${FOLDER_NAME}/out  ]; then mkdir  "${FOLDER_NAME}/out"; fi
cd "${FOLDER_NAME}"
cmake ${CMAKE_ARGS} ..
make
cd "$current_dir"
