#ifndef SDP_CONTAINER_SRC_LIST_HPP
#define SDP_CONTAINER_SRC_LIST_HPP

#include "GenericTypes.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

/**
 * Template list interface
 */
template <typename T>
class List {
 public:
  virtual bool Add(T&, const unum) = 0;
  virtual bool Add(T&) = 0;
  virtual bool Contains(CONDITION predicate) const = 0;
  virtual bool Contains(const T&) const = 0;
  virtual void Clear() = 0;
  virtual void Filter(CONDITION predicate) = 0;
  virtual const T& Get(unum) const = 0;
  virtual unum GetSize() const = 0;
  virtual bool IsEmpty() const = 0;
  virtual void Print(std::ostream& out) const = 0;
  virtual void Sort() = 0;
  virtual T Remove(const unum) = 0;
};

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_LIST_HPP