#ifndef SDP_CONTAINER_TEST_TYPED_TEST_HELPER_HPP
#define SDP_CONTAINER_TEST_TYPED_TEST_HELPER_HPP

#include <gtest/gtest.h>
#include <stdlib.h>
#include <time.h>

#include <iterator>

#define MAGIC_LEN (1 << 10)
#define MAGIC_LEN_SMALL (1 << 2)

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {
namespace test {

typedef testing::Types<int, double> NumericImplementations;

template <typename T>
class TypedTestHelper : public testing::Test {
 public:
  TypedTestHelper() {
    srand(time(NULL));
    const int halfRandomMax = RAND_MAX / 2;
    for (unum i = 0; i < MAGIC_LEN; i++) x[i] = rand() - halfRandomMax;

    // WARNING: Check if evevy element has same module by 2
    y[0] = (T)19.4;
    y[1] = (T)13;
    y[2] = (T)17;
    y[3] = (T)15.1;
  }

  ~TypedTestHelper() {
    delete[] this->x;
    delete[] this->y;
  }

  T* x = new T[MAGIC_LEN];
  T* y = new T[MAGIC_LEN_SMALL];
};

}  // namespace test
}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu
#endif  // SDP_CONTAINER_TEST_TYPED_TEST_HELPER_HPP