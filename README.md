# SDP Containers

Project for SDP course\
Task for this project can be fount in [Task.pdf](https://gitlab.com/su-fmi-pecata/sdp-containers/blob/master/Task.pdf) and [Task.txt](https://gitlab.com/su-fmi-pecata/sdp-containers/blob/master/Task.txt).

### How to build the project
I have created build script `build.sh` in root directory of the project. You can use it to build and compile the app (Shame!). It comes with some built-in options such as:
- `-d` | `--debug` -> Used for compiling test
- `-g` | `--update-gtest` -> Used for downloading and linking google test for current project (Requires `--debug` option)
- `-c` | `--clear` -> Used for removing build folder
> WARNING: This script requires `/usr/bin/getopt` bin.

### How to start the project
When project is compiled you can run it but starting `build/main` file.\
You can run all test from `build/test_project`.