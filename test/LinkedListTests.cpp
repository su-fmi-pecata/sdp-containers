#include <gtest/gtest.h>

#include <exception>

#include "../src/LinkedList.hpp"
#include "../src/ListIterator.hpp"
#include "./TypedTestsHelper.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {
namespace test {

template <typename T>
class LinkedListTests : public TypedTestHelper<T> {
 public:
  LinkedList<T> list;
};

TYPED_TEST_SUITE(LinkedListTests, NumericImplementations);

TYPED_TEST(LinkedListTests, GetSize__Should_ReturnSize0_When_Empty) {
  ASSERT_EQ(this->list.GetSize(), 0);
}

TYPED_TEST(LinkedListTests, IsEmpty__Should_BeEmpty_When_Empty) {
  ASSERT_TRUE(this->list.IsEmpty());
}

TYPED_TEST(LinkedListTests, Get__Should_ThrowException_When_Empty) {
  ASSERT_THROW(this->list.Get(0), std::out_of_range);
}

TYPED_TEST(LinkedListTests, Remove__Should_ThrowException_When_Empty) {
  ASSERT_THROW(this->list.Remove(0), std::out_of_range);
}

TYPED_TEST(LinkedListTests, Add_Get__Should_AddElementsToListBack) {
  for (unum i = 0; i < MAGIC_LEN; i++) {
    this->list.Add(this->x[i], i);
    ASSERT_DOUBLE_EQ(this->x[i], this->list.Get(i));
  }
  ASSERT_EQ(this->list.GetSize(), MAGIC_LEN);
}

TYPED_TEST(LinkedListTests, Add_Get__Should_AddElementsToListFront) {
  for (unum i = 0; i < MAGIC_LEN; i++) {
    this->list.Add(this->x[i], 0);
    ASSERT_DOUBLE_EQ(this->x[i], this->list.Get(0));
  }
  for (unum i = 0; i < MAGIC_LEN; i++) {
    ASSERT_DOUBLE_EQ(this->x[i], this->list.Get(MAGIC_LEN - 1 - i));
  }
  ASSERT_EQ(this->list.GetSize(), MAGIC_LEN);
}

TYPED_TEST(LinkedListTests, Reverse__Should_ReverseList) {
  // insert front
  for (unum i = 0; i < MAGIC_LEN; i++) {
    this->list.Add(this->x[i], 0);
  }

  // normalize == insert bask
  this->list.Reverse();
  for (unum i = 0; i < MAGIC_LEN; i++) {
    ASSERT_DOUBLE_EQ(this->x[i], this->list.Get(i));
  }

  // denormalize == insert front
  this->list.Reverse();
  for (unum i = 0; i < MAGIC_LEN; i++) {
    ASSERT_DOUBLE_EQ(this->x[i], this->list.Get(MAGIC_LEN - 1 - i));
  }
}

TYPED_TEST(LinkedListTests,
           Contains_Elem__Should_ReturnTrue_When_ContainsElement) {
  // insert front
  for (unum i = 0; i < MAGIC_LEN; i++) {
    this->list.Add(this->x[i], 0);
  }

  for (unum i = 0; i < MAGIC_LEN; i++) {
    ASSERT_TRUE(this->list.Contains(this->x[i]));
  }
}

TYPED_TEST(LinkedListTests,
           Contains_Elem__Should_ReturnFalse_When_NotContainsElement) {
  for (unum i = 0; i < MAGIC_LEN_SMALL; i++) {
    this->list.Add(this->y[i]);
    this->y[i] += 1;
    ASSERT_FALSE(this->list.Contains(this->y[i]));
  }
}

TYPED_TEST(LinkedListTests,
           Contains_Predicate__Should_ReturnTrue_When_ContainsElement) {
  // insert front
  for (unum i = 0; i < MAGIC_LEN; i++) {
    this->list.Add(this->x[i], 0);
  }

  for (unum i = 0; i < MAGIC_LEN; i++) {
    TypeParam p = this->x[i];
    ASSERT_TRUE(
        this->list.Contains([&p](const TypeParam& s) { return p == s; }));
  }
}

TYPED_TEST(LinkedListTests,
           Contains_Predicate__Should_ReturnFalse_When_NotContainsElement) {
  for (unum i = 0; i < MAGIC_LEN_SMALL; i++) {
    this->list.Add(this->y[i]);
    TypeParam p = this->y[i] + 1;
    ASSERT_FALSE(
        this->list.Contains([&p](const TypeParam& s) { return p == s; }));
  }
}

TYPED_TEST(LinkedListTests,
           ForwardIterator_Should_GetEveryElementInCorrectOrder) {
  for (unum i = 0; i < MAGIC_LEN; i++) {
    this->list.Add(this->x[i]);
  }

  int i = 0;
  for (auto it = this->list.Begin(); it != this->list.End(); it++) {
    ASSERT_DOUBLE_EQ(this->x[i], (TypeParam)it);
    i++;
  }
}

TYPED_TEST(LinkedListTests,
           ForwardIterator_Should_GetEveryElementInReversedOrder) {
  for (unum i = 0; i < MAGIC_LEN; i++) {
    this->list.Add(this->x[i]);
  }

  int i = MAGIC_LEN - 1;
  auto it = this->list.Last();
  while (true) {
    ASSERT_DOUBLE_EQ(this->x[i], (TypeParam)it);
    if (!it.HasPrev()) break;
    it--;
    i--;
  }
}

}  // namespace test
}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu