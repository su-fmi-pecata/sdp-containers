#ifndef SDP_CONTAINER_SRC_HETEROGEN_LIST_ITERATOR_SEQUENCE_HPPSequence
#define SDP_CONTAINER_SRC_HETEROGEN_LIST_ITERATOR_SEQUENCE_HPPSequence

#include <functional>

#include "HeterogenList.hpp"
#include "Iterator.hpp"
#include "ListIterator.hpp"
#include "containers/Container.hpp"
#include "containers/ContainerIterator.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {

template <typename T>
class HeterogenList;

template <typename T>
class HeterogenListIteratorSequence
    : public Iterator<T, HeterogenListIteratorSequence<T>> {
 private:
  friend class HeterogenList<T>;

  void Search(
      const ListIterator<Container<T>*>& start,
      const ListIterator<Container<T>*>& end,
      std::function<void(ListIterator<Container<T>*>&, ContainerIterator<T>&)>
          manipulate) const {
    ListIterator<Container<T>*> ctrIter = start;

    bool saveMeCircleList = true;
    while (saveMeCircleList) {
      saveMeCircleList = false;
      for (ctrIter; ctrIter.Valid() && ctrIter != end; ++ctrIter) {
        Container<T>* ctr = ctrIter;
        ContainerIterator<T> tmp = ctr->Begin();
        for (tmp; tmp != ctr->End(); ++tmp) {
          manipulate(ctrIter, tmp);
        }
      }
      if (!ctrIter.Valid() && ctrIter != end) {
        saveMeCircleList = true;
        ctrIter = firstContainer;
      }
    }
  }

  ListIterator<Container<T>*> firstContainer;
  ListIterator<Container<T>*> startedCurrentValueContainer;
  ListIterator<Container<T>*> container;
  ContainerIterator<T> currentElement;
  const T currentValue;

 public:
  HeterogenListIteratorSequence(
      ListIterator<Container<T>*> firstContainer,
      ListIterator<Container<T>*> startedCurrentValueContainer,
      ListIterator<Container<T>*> container,
      ContainerIterator<T> currentElement, const T currentValue)
      : firstContainer(firstContainer),
        startedCurrentValueContainer(startedCurrentValueContainer),
        container(container),
        currentElement(currentElement),
        currentValue(currentValue) {}

  virtual T& Get() const { return currentElement; }
  virtual bool Valid() const { return currentElement.Valid(); }

  virtual HeterogenListIteratorSequence<T> Next() const;
  virtual HeterogenListIteratorSequence<T> Prev() const;

  virtual bool operator==(const HeterogenListIteratorSequence<T>& it) const {
    return container == it.container && currentElement == it.currentElement;
  }
};
template <typename T>
HeterogenListIteratorSequence<T> HeterogenListIteratorSequence<T>::Next()
    const {
  if (currentElement.HasNext() && (T)currentElement.Next() == currentValue) {
    return HeterogenListIteratorSequence<T>(
        firstContainer, startedCurrentValueContainer, container,
        currentElement.Next(), currentValue);
  }
  // there is no such value in this container, should search new one...
  ListIterator<Container<T>*> ctrIter = container.Next();
  ContainerIterator<T> elemIter = currentElement.Next();

  // findSame
  bool found = false;
  Search(ctrIter, startedCurrentValueContainer,
         [&ctrIter, &elemIter, &found, val = currentValue](
             ListIterator<Container<T>*>& ctrIterValue,
             ContainerIterator<T>& elemIterValue) {
           if (found) return;
           if (elemIterValue == val) {
             elemIter = elemIterValue;
             ctrIter = ctrIterValue;
           }
         });

  if (found) {
    return HeterogenListIteratorSequence<T>(firstContainer,
                                            startedCurrentValueContainer,
                                            ctrIter, elemIter, currentValue);
  }

  // find next min
  ctrIter = firstContainer.Prev();
  elemIter = ((Container<T>*)container)->End();
  Search(ctrIter, startedCurrentValueContainer,
         [&ctrIter, &elemIter, &found, val = currentValue](
             ListIterator<Container<T>*>& ctrIterValue,
             ContainerIterator<T>& elemIterValue) {
           if (elemIterValue > val) {
             if (ctrIter.Valid() && elemIter.Valid()) {
               if ((T)elemIterValue < (T)elemIter) {
                 elemIter = elemIterValue;
                 ctrIter = ctrIterValue;
               }
             } else {
               elemIter = elemIterValue;
               ctrIter = ctrIterValue;
             }
           }
         });

  return HeterogenListIteratorSequence<T>(firstContainer,
                                          startedCurrentValueContainer, ctrIter,
                                          elemIter, currentValue);

  //   if (container.HasNext()) {
  //     return HeterogenListIteratorSequence<T>(n, n.Get()->Begin());
  //   }

  //   return HeterogenListIteratorSequence<T>(n, currentElement.Next());
}
template <typename T>
HeterogenListIteratorSequence<T> HeterogenListIteratorSequence<T>::Prev()
    const {
  if (currentElement.HasNext()) {
    return HeterogenListIteratorSequence<T>(container, currentElement.Next());
  }

  ListIterator<Container<T>*> n = container.Next();
  if (container.HasNext()) {
    return HeterogenListIteratorSequence<T>(n, n.Get()->Begin());
  }

  return HeterogenListIteratorSequence<T>(n, currentElement.Next());
}

}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu

#endif  // SDP_CONTAINER_SRC_HETEROGEN_LIST_ITERATOR_SEQUENCE_HPPSequence