#include <gtest/gtest.h>

#include "../../src/containers/Stack.hpp"
#include "./ContainerTests.hpp"

namespace eu {
namespace petartoshev {
namespace fmi {
namespace sdp {
namespace containers {
namespace test {

template <typename T>
class StackTests : public ContainerTests<T> {
 public:
  StackTests()
      : ContainerTests<T>(
            *(new eu::petartoshev::fmi::sdp::containers::Stack<T>)) {}
};

TYPED_TEST_SUITE(StackTests, NumericImplementations);

#define TEST_PARENT StackTests
#include "./ContainerTestsHelper.hpp"

TYPED_TEST(StackTests, Add_Remove__Should_AddElements) {
  for (unum i = 0; i < MAGIC_LEN; i++)
    ASSERT_TRUE(this->container.Add(this->x[i]));
  ASSERT_EQ(this->container.GetSize(), MAGIC_LEN);

  for (unum i = 0; i < MAGIC_LEN; i++)
    ASSERT_DOUBLE_EQ(this->x[MAGIC_LEN - i - 1], this->container.Remove());
  ASSERT_EQ(this->container.GetSize(), 0);
}

}  // namespace test
}  // namespace containers
}  // namespace sdp
}  // namespace fmi
}  // namespace petartoshev
}  // namespace eu