#include <iostream>

#include "HeterogenList.hpp"

int main() {
  eu::petartoshev::fmi::sdp::containers::HeterogenList<int> l;
  std::cout << "Enter input file path:" << std::endl;
  const int MAX_FILENAME_LENGTH = 256;
  char file[MAX_FILENAME_LENGTH];
  std::cin.getline(file, MAX_FILENAME_LENGTH);
  l.Read(file);
  l.Print(std::cout);
  std::cout << "Enter output file path:" << std::endl;
  std::cin.getline(file, MAX_FILENAME_LENGTH);
  l.Save(file);

  return 0;
}